# AML-Evaluation

Applied machine learning algorithm evaluation.

Requires a copy of the UCI Breast Cancer Wisconsin (Diagnostic) Data Set named `wdbc.data`. [Available Here](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Diagnostic%29)

